package com.voroj.reqres.in;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReqresTest {
    private final static String URL = "https://reqres.in/";

    @DisplayName("Проверить успешную регистрацию")
    @Test
    public void successRegister() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(200));
        Integer id = 4;
        String token = "QpwL5tke4Pnpja7X4";
        Registration user = new Registration("eve.holt@reqres.in", "pistol");
        SuccessReg successReg = given()
                .body(user)
                .when()
                .post("api/register")
                .then().log().all()
                .extract().as(SuccessReg.class);
        assertEquals(id, successReg.getId());
        assertEquals(token, successReg.getToken());
    }

    @DisplayName("Проверить соответствие id с номером avatar")
    @Test
    public void checkAvatarAndIdTest() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(200));
        List<UserData> users = given()
                .when()
                .get("api/users?page=2")
                .then().log().all()
                .extract().body().jsonPath().getList("data", UserData.class);
//        users.stream().forEach(x-> Assertions.assertTrue(x.getAvatar().contains(x.getId().toString())));
//        Assertions.assertTrue(users.stream().allMatch(x->x.getEmail().endsWith("@reqres.in")));
        List<String> avatars = users.stream().map(UserData::getAvatar).collect(Collectors.toList());
        List<String> ids = users.stream().map(x -> x.getId().toString()).collect(Collectors.toList());
        for (int i = 0; i < avatars.size(); i++) {
            assertTrue(avatars.get(i).contains(ids.get(i)));
        }
    }

    @DisplayName("Проверить неуспешную регистрацию")
    @Test
    public void unSuccessRegister() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(400));
        Registration user = new Registration("sydney@fife", "");
        UnSuccess unSuccess = given()
                .body(user)
                .when()
                .post("api/register")
                .then().log().all()
                .extract().as(UnSuccess.class);
        assertEquals("Missing password", unSuccess.getError());

    }

    @DisplayName("Проверить сортировку данных по возрасту")
    @Test
    public void sortedTest() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(200));
        List<ColorsData> colors = given()
                .when()
                .get("api/unknown")
                .then().log().all()
                .extract().body().jsonPath().getList("data", ColorsData.class);
        List<Integer> years = colors.stream().map(ColorsData::getYear).collect(Collectors.toList());
        List<Integer> sortedYears = years.stream().sorted().collect(Collectors.toList());
        assertEquals(sortedYears, years);
        System.out.println(years);
        System.out.println(sortedYears);
    }

    @DisplayName("Проверить удаление пользователя")
    @Test
    public void deleteTest() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(204));
        given()
                .when()
                .delete("api/users/2")
                .then().log().all();

    }

    @DisplayName("Проверить успешное создание нового пользователя")
    @Test
    public void createNewUserTest() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(201));
        String name = "morpheus";
        String job = "leader";
        NewUser newUser = new NewUser(name, job);
        SuccessNewUser successNewUser = given()
                .body(newUser)
                .when()
                .post("api/users")
                .then().log().all()
                .extract().as(SuccessNewUser.class);
        assertEquals(successNewUser.getName(), name);
        assertEquals(successNewUser.getJob(), job);
    }

    @DisplayName("Проверить получение несуществующего пользователя")
    @Test
    public void singleUserNotFoundTest() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(404));
        given()
                .when()
                .get("api/users/23")
                .then().log().all();
    }

    @DisplayName("Проверить обновление данных о пользователе")
    @Test
    public void updateTest() {
        Specifications.installSpec(Specifications.requestSpecification(URL),
                Specifications.responseSpecificationUnique(200));
        String name = "morpheus";
        String job = "zion resident";
        NewUser newUser = new NewUser("morpheus", "zion resident");
        UpdateUser user = given()
                .body(newUser)
                .when()
                .put("api/users/2")
                .then().log().all()
                .extract().as(UpdateUser.class);
        assertEquals(name, user.getName());
        assertEquals(job, user.getJob());

    }
}
