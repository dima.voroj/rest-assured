package com.voroj.reqres.in;

public class NewUser {
    private String name;
    private String job;

    public NewUser(String name, String job) {
        this.name = name;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }

    public NewUser() {

    }

}
