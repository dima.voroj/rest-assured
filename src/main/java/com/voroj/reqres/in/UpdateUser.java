package com.voroj.reqres.in;

import java.util.Date;

public class UpdateUser {
    private String name;
    private String job;
    private Date updatedAt;

    public UpdateUser(String name, String job, Date updatedAt) {
        this.name = name;
        this.job = job;
        this.updatedAt = updatedAt;
    }

    public UpdateUser() {

    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }
}
