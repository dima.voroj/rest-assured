package com.voroj.reqres.in;

public class UnSuccess {
    private String error;

    public UnSuccess(String error) {
        this.error = error;
    }

    public UnSuccess() {

    }

    public String getError() {
        return error;
    }
}
